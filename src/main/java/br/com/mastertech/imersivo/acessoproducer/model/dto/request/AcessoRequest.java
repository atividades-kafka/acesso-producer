package br.com.mastertech.imersivo.acessoproducer.model.dto.request;

public class AcessoRequest {

    private long porta;
    private long cliente;

    public long getPorta() {
        return porta;
    }

    public void setPorta(long porta) {
        this.porta = porta;
    }

    public long getCliente() {
        return cliente;
    }

    public void setCliente(long cliente) {
        this.cliente = cliente;
    }
}
