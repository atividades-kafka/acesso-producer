package br.com.mastertech.imersivo.acessoproducer.service;

import br.com.mastertech.imersivo.acessoproducer.model.Acesso;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import java.util.Random;

@Component
public class AcessoKafkaProducerService {

    @Autowired
    private KafkaTemplate<String, Acesso> sender;

    public void enviarAcesso(Acesso acesso) {
        if (getBoolean()) {
            sender.send("acesso", "1", acesso );
        }
    }

    private boolean getBoolean() {
        Random random = new Random();
        return random.nextBoolean();
    }

}
