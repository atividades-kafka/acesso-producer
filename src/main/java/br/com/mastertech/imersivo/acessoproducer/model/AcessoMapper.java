package br.com.mastertech.imersivo.acessoproducer.model;

import br.com.mastertech.imersivo.acessoproducer.model.dto.request.AcessoRequest;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

@Component
public class AcessoMapper {

    public Acesso toAcesso(long cliente, long porta) {
        Acesso acesso = new Acesso();
        acesso.setCliente(cliente);
        acesso.setPorta(porta);
        acesso.setTimestamp(formatDate(Calendar.getInstance()));

        return acesso;
    }

    private String formatDate(Calendar calendar) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        return sdf.format(calendar.getTime());
    }
}
