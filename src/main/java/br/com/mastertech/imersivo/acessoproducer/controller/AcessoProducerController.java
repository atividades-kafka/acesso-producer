package br.com.mastertech.imersivo.acessoproducer.controller;

import br.com.mastertech.imersivo.acessoproducer.model.Acesso;
import br.com.mastertech.imersivo.acessoproducer.model.AcessoMapper;
import br.com.mastertech.imersivo.acessoproducer.model.dto.request.AcessoRequest;
import br.com.mastertech.imersivo.acessoproducer.service.AcessoKafkaProducerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class AcessoProducerController {

    @Autowired
    private AcessoKafkaProducerService producer;

    @Autowired
    private AcessoMapper mapper;

    @GetMapping(value = "/acesso/{cliente}/{porta}")
    public void enviarAcesso(@PathVariable long cliente, @PathVariable long porta) {
        Acesso acesso = mapper.toAcesso(cliente, porta);
        producer.enviarAcesso(acesso);
    }


}
