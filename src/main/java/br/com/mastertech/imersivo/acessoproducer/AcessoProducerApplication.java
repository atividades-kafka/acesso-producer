package br.com.mastertech.imersivo.acessoproducer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AcessoProducerApplication {

	public static void main(String[] args) {
		SpringApplication.run(AcessoProducerApplication.class, args);
	}

}
