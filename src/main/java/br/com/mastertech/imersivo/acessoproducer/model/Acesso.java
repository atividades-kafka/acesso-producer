package br.com.mastertech.imersivo.acessoproducer.model;

import java.util.Calendar;

public class Acesso {

    private long porta;
    private long cliente;
    private String timestamp;

    public long getPorta() {
        return porta;
    }

    public void setPorta(long porta) {
        this.porta = porta;
    }

    public long getCliente() {
        return cliente;
    }

    public void setCliente(long cliente) {
        this.cliente = cliente;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
}
